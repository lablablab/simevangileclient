﻿/*

This script is used to display and fade titles & logos. Attach it to an empty that you will use as a title object. 
Mostly useful for intros/outros. Could be easily modded to display on bool triggers...

*/

#pragma strict

	// Hide the GUIStyle panel but let the user set the parameter that are needed.
	@HideInInspector
	var titleGuiStyle : GUIStyle;
	
	// Set the GUIStyle parameters
	titleGuiStyle.font = textFont ;
	titleGuiStyle.normal.textColor = textColor;
	titleGuiStyle.normal.textColor.a = 1;
	titleGuiStyle.alignment = TextAnchor.MiddleCenter;
	
	// Menu items in the inspector
	// Logo	
	var displayLogo : boolean = true;
	var logoImage : Texture;
	@Range (0.00, 10.00)
	var logoSize : float = 1;
	@Range (-5, 5)  
	var logoXoffset : float = 1;
	@Range (-5, 5)  
	var logoYoffset : float = 1;
	
	//Text
	var displayText : boolean = true;
	var titleText : String = "so ulmash...";
	var textFont : Font;
	var textColor : Color;
	@Range (0.00, 10.00)
	var textSize : float = 1;
	@Range (-5, 5) 
	var textXoffset : float = 1;
	@Range (-5, 5)  
	var textYoffset : float = 1;
	private var textPosX = Screen.width/4;
	private var textPosY = Screen.height/3;
	private var fontSize = Screen.width/25;
	private var imageSize = Screen.width/2.5;
	
	//Fade
	@HideInInspector
	var gA : float = 0.0; //Master GUI alpha value
	GUI.color.a = gA; 
	var fadeToggle = true;
	@Range (0.0000, 0.01)
	var fadeSpeed = 0.005;
	var displayDelay = 0f;
	var displayTime = 1f;
	fontSize = fontSize*textSize;
	imageSize = logoSize*imageSize;
	titleGuiStyle.fontSize = fontSize;
	
	function Awake(){
	gA = 0.0; //Make sure that values are resetted
	}
	
	function Start() {
	FadeIn();
	}
			
	// Is drawn on every frame & everything here can be adjusted from the inspector.						
	function OnGUI () {
	GUI.color.a = gA; // Constantly update alpha values
		if ( displayLogo )
			GUI.Label (Rect (Screen.width/2-imageSize/2*logoXoffset, Screen.height/2-imageSize/2*logoYoffset, imageSize, imageSize), logoImage, titleGuiStyle);
		if ( displayText )
			GUI.Label (Rect (Screen.width/2-textPosX/2*textXoffset, Screen.height/2*textYoffset, textPosX, textPosY), titleText, titleGuiStyle);
	}

	// Function that fade the titles
	function FadeIn() {
		yield WaitForSeconds(displayDelay); //Seconds before the titles appears
	if (!fadeToggle)
		fadeSpeed = 1;
		for (var f = 0.0; f <= 1.0; f += fadeSpeed) { // Ramp the alpha
		    gA = f;
		    yield WaitForSeconds(fadeSpeed);
		}
		gA = 1.0;
	    yield WaitForSeconds(displayTime); // Number of second the titles is displayed at 1.0 alpha
	    FadeOut(); //Call back to FadeOut() once it's done
	}
	
	function FadeOut() {
	    for (var f = 1.0; f >= 0.0; f -= fadeSpeed) {
	        gA = f;
	        yield WaitForSeconds(fadeSpeed);
	    }
	    gA = 0.0;
	}
	