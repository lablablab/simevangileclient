﻿#pragma strict

private var sr:SpriteRenderer;
public var frames:Sprite[];

function Start(){
	sr = transform.root.GetComponent(SpriteRenderer);
	
}

public function changeState(curValue:float, MaxValue:float){

	if (curValue<0){curValue=0;}
	if (curValue>MaxValue){curValue=MaxValue;}
	var newFrame:int = Mathf.Round((curValue/MaxValue)*16);
	//print("frame:"+newFrame);
	sr.sprite = frames[newFrame];
	

}