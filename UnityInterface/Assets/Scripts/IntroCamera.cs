/*	
 *	Used to fade in/out of scene after a delay. Useful for intro/outro/cutscenes 
 *	
 * 	Depend on the iTween plugin for Unity.
 * 	
 */

using UnityEngine;
using System.Collections;

public class introCamera : MonoBehaviour
{	

	public bool fadeInToggle = true;
	public float fadeInDelay = 2.0f;
	public float fadeTime = 2.0f;
	public float displayDuration = 1.0f;
	public bool fadeOutToggle = true;


	void Start ()
	{
		iTween.CameraFadeAdd();
		fadeIn();
		Invoke("fadeOut", displayDuration+fadeInDelay+fadeTime); //Used to call a function after n-seconds : Invoke("method", int-float)
	}

	void fadeIn()
	{   if(fadeInToggle)
			iTween.CameraFadeFrom (iTween.Hash ("delay", fadeInDelay, "time", fadeTime, "alpha", 1));
	}

	void fadeOut()
	{
		if(fadeOutToggle)
			iTween.CameraFadeTo (iTween.Hash("time", fadeTime, "alpha", 0, "amount", 1));
	}
}

