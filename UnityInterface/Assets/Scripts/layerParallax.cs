﻿/*	
 *	Used to move/fade layers around. Useful to create flash-like transition and animations. Attach it to container or objects that you want to move.
 *	
 * 	Depend on the iTween plugin for Unity.
 * 	
 */

using UnityEngine;
using System.Collections;

public class layerParallax : MonoBehaviour {

	// All values are superseeded by tweaks in the inspector
	public float delay = 1.0f; // Wait before starting
	public float duration = 10.0f; // Duration of the transition
	public float xTranslateBy = 0f; // Translation position
	public string easeType = "linear"; 	// See http://www.robertpenner.com/easing/easing_demo.html
	public bool pingPong = false; // Pingpong will move the layer until it reach its end, then send it the other way
	
	// Start is called once when object is created
	void Start() {
		layerMove (pingPong);
	}

	// Update is called once per frame
	void Update () {
	}

	void layerMove(bool _pingPong)
	{
		if (_pingPong) {
			// See http://itween.pixelplacement.com/documentation.php
			iTween.MoveAdd (gameObject, iTween.Hash ("x", xTranslateBy, "easeType", easeType, "time", duration, "delay", delay, "looptype", "pingPong" ));
		} else {
			iTween.MoveAdd (gameObject, iTween.Hash ("x", xTranslateBy, "easeType", easeType, "time", duration, "delay", delay));
		}
	}

}
