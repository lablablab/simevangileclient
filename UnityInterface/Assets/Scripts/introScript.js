﻿#pragma strict

/*

This script adds a skip and a fullscreen button, along with sounds when user click them. It uses iTween to fade to the next scene. 
Attach it to an empty that you will use as a GUILayer
Useful mostly for intros/outros.

Depend on the iTween plugin for Unity.

*/


// Hide the GUIStyle panel but let the user set the parameter that are needed.
@HideInInspector
var btnStyle : GUIStyle;

var buttonFont : Font;
btnStyle.font = buttonFont;

// Dimention and status of the "skip" button
private var btnWidth : int = Screen.width/20;
private var btnHeight : int = Screen.height/20;
private var skipBtn : boolean;

// Fullscreen button
var fullScreenIcon : Texture;
private var fullScreenBtn : boolean;

@HideInInspector
var menuStyle : GUIStyle;

menuStyle.fontSize = Screen.width/50;

//Array to make a random bank of sound variations. 
public var buttonPressSfx : AudioClip[]; 

// User agent bool used to "fix" a Chrome/Unity bug
private var chromeBrowser : boolean = false;


function Start ()
{	
	iTween.CameraFadeAdd();
  #if UNITY_WEBPLAYER
    testBrowser();
  #endif
}

function Update(){
	
	// Fade out to next scene if a key is press, the skipBtn is pressed or after 30 seconds
	if(Time.time >= 30 || Input.anyKey || skipBtn){
		// Particular function with iTween: hash table with a "oncomplete" call-back to loadmenu 
		iTween.CameraFadeTo(iTween.Hash("time", 1f, "alpha", 1,"onComplete","loadMenu","onCompleteTarget",gameObject));
	}
	
	// Toggle fullscreen
/*	if(fullScreenBtn) {
		playSound();
		Screen.fullScreen = !Screen.fullScreen;
	}*/
}

function OnGUI ()
{
	// "Press any key" button
	GUI.BeginGroup (new Rect (0, Screen.height-(btnHeight+(btnHeight/8)), Screen.width, btnHeight));
	skipBtn = GUI.Button(new Rect (Screen.width/2-btnWidth,0,btnWidth*2,menuStyle.fontSize*2), "[press any key]", menuStyle);
	GUI.EndGroup ();
	
	// "Fullscreen" button
/*	GUI.BeginGroup (new Rect ((1240* Screen.width / 1280) - btnWidth/10, (720 * Screen.height / 800)- btnHeight/10, btnWidth, btnHeight));
	fullScreenBtn = GUI.Button(new Rect (0,0,btnWidth,btnHeight), fullScreenIcon, btnStyle);
	GUI.EndGroup ();*/
};

// Play sounds fx choosen randomly from the array.
function playSound() {
	if (buttonPressSfx.Length >= 1 && buttonPressSfx[0]) {
	AudioSource.PlayClipAtPoint(buttonPressSfx[Random.Range(0, buttonPressSfx.Length)], transform.position);
	}
}

// Load the next scene (in this case, the menu)
function loadMenu(){
	playSound();
    Application.LoadLevel("MainMenu");
}

// Fire the isBrowserChrome() js function on the webpage
function testBrowser() {
	Application.ExternalEval ("isBrowserChrome()");
}

// Is called back by js only if isBrowserChrome() test true
function browserIsChrome( param : String ) {
	Application.ExternalEval ("reloadUnity()");
}