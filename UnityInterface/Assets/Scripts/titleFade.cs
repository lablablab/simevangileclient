﻿using UnityEngine;
using System.Collections;

public class titleFade : MonoBehaviour {
	public float startDelay = 2.0f;
	public float fadeDelay = 1.0f;
	public float displayDuration = 1.0f;
	public bool fadeInOutToggle = true;

	void Start ()
	{
		if (fadeInOutToggle) {
						fadeIn ();
				}
	}
	
	void fadeIn()
	{
		iTween.FadeFrom(gameObject, iTween.Hash("delay", startDelay, "time", fadeDelay, "alpha", 0, "oncomplete", "fadeOut"));
	}
	
	void fadeOut()
	{
		iTween.FadeTo(gameObject, iTween.Hash("delay", displayDuration, "time", fadeDelay, "alpha", 0));
	}

}


