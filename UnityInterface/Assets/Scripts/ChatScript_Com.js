﻿#pragma strict
// private var ChatScripUrl= "http://enurai.encs.concordia.ca/chatbot/chatscriptclient.php?";
// private var NewIDUrl= "http://enurai.encs.concordia.ca/chatbot/chatscriptid.php?";
// private var ChatScripUrl= "http://127.0.0.1/sp_client.php?";
// private var NewIDUrl= "http://127.0.0.1/sp_id.php?";
private var ChatScripUrl= "http://167.160.163.209/lablablab/sp_client.php?";
private var NewIDUrl= "http://167.160.163.209/lablablab/sp_id.php?";
private var userID = "globule";
private var userNumber:int = 0;
private var userInput : String = "[Type your answer here]";
private var userText : String = "Hello young lady";
private var botOutput : String = null;
private var consoleText : String = "";
private var scrollPosition : Vector2 = Vector2.zero;
private var showLog : boolean = false;
private var ambarWin:boolean = false;
private var moutonWin:boolean = false;
private var noWin:boolean = false;


// Text bubble variables
public var textDelay = 0.2;
private var botWords : String = "Testing, testing, one two, one two";
private var botCurrentWords : String = "";
private var botBubbleAlpha:float = 0;
private var playerWords : String = "Testing, testing, one two, one two";
private var playerCurrentWords : String = "";
private var sheepWords : String = "Testing, testing, one two, one two";
private var sheepCurrentWords : String = "";
private var sheepBubbleAlpha:float = 0;
private var coupe:boolean= false;

// Flags
private var botTalking : boolean = false;
private var playerTalking : boolean = false;
private var sheepTalking : boolean = false;
private var waitingForMessage: boolean = false;
private var waitingForId: boolean = false;


// GUI oui oui
var inputBoxCoords : Rect;
var inputBoxStyle : GUIStyle;
var logBoxStyle : GUIStyle;
var logCoords:Rect;
var logTextStyle:GUIStyle;
var textBubbleStyle : GUIStyle;
var textBubbleStyle2 : GUIStyle;
var textBubbleCoords : Rect;
var userBubbleCoords : Rect;
var sheepBubbleCoords :Rect;
var buttonStyle : GUIStyle;
var restartButtonCoords : Rect;
var logButtonCoords : Rect;
var trustLabelCoords : Rect;
var sendButtonCoords:Rect;
var patienceLabelCoords : Rect;
var labelStyle : GUIStyle;
var fullScreenIcon : Texture;
public var TrustBar : ProgressBar;
public var PatienceBar : ProgressBar;
public var AureolesAmbar : aureoleSprite;
public var AureolesMoutons : aureoleSprite;
var inputWindow:UnityEngine.UI.InputField;


//Sounds
public var ambienceSound:AudioClip;
public var sfxError:AudioClip[];
public var sfxAction:AudioClip[];
public var sfxTrust:AudioClip[];
public var sfxMoutons:AudioClip[];
public var sfxEndGame:AudioClip[];


//Game variables
private var session : int = 0;
private var ambar : int = 0;
private var maxAmbar : int = 7;
private var mouton : int = 0;
private var maxMouton : int = 7;
private var gambit1:boolean = false;
private var gambit2:boolean = false;
private var gambit3:boolean = false;
private var gambit4:boolean = false;
private var gambit5:boolean = false;
private var mouton1:boolean = false;
private var mouton2:boolean = false;
private var mouton3:boolean = false;
private var mouton4:boolean = false;
private var mouton5:boolean = false;
private var endGame:boolean = false;
private var curGambits:float = 0;
private var maxGambits:float = 20;
var nuit:SpriteRenderer;


function Awake(){
	nuit.color.a = curGambits;
}

function Start ()
{
	yield getNewID ();
	initConversation ();	
}


function Update(){
	if(Input.GetMouseButtonDown(0)&&(botCurrentWords.Length>3)){
		botCurrentWords = botWords;
		coupe = true;
	}
	if (playerTalking == true || botTalking == true || sheepTalking == true || waitingForMessage == true){
		inputWindow.interactable=false;
	}
	else if (!(playerTalking == true || botTalking == true || sheepTalking == true || waitingForMessage == true)){
		inputWindow.interactable=true;

	}
	

}

function botSays (bubbletext : String, pause:boolean)
{
    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
    bubbletext = parseCodes (bubbletext);
    if(moutonWin){bubbletext="Well I...";}
	while (playerTalking == true || botTalking == true || sheepTalking == true || waitingForMessage == true) 
	{
		yield WaitForSeconds (0.1);
	}
	
    botTalking = true;
    botWords = bubbletext;
	botCurrentWords = "";
	consoleText = consoleText+"\n\n[Ambar]: "+botWords;
	scrollPosition.y = Mathf.Infinity; 	
	
    for (var letter in bubbletext.ToCharArray ())
    {
        if (botCurrentWords == bubbletext) break;
        if (botWords != bubbletext) break;
        botCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.35, 0.55)); // Original Random.Range(0.5, 2)
    }
    
    if(pause||ambarWin||noWin||moutonWin){
	    coupe = false;
	    for(var i:int=0;i<10;i++){
	    	if(coupe==true){
	    		break;
	    	}
	    	yield(WaitForSeconds(0.15));  	
	    }
	}
	    
    botTalking = false;    
    //Set focus on input window
    inputWindow.interactable=true;
    inputWindow.ActivateInputField();	
    if(ambarWin){endGameAmbar();}
    if(moutonWin){endGameMouton();}
    if(noWin){endGameBof();}

}

function sheepSays (bubbletext : String)
{
    
    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
    bubbletext = parseCodes (bubbletext);
    
   
	while (playerTalking == true || botTalking == true || sheepTalking == true) 
	{
		yield WaitForSeconds (0.1);
	}
	
    sheepTalking = true;
    sheepWords = bubbletext;
	sheepCurrentWords = "";
	
	//update log:
	consoleText = consoleText+"\n\n[Sheep]: "+sheepWords;
	scrollPosition.y = Mathf.Infinity; 	
	
    for (var letter in bubbletext.ToCharArray ())
    {
        if (sheepCurrentWords == bubbletext) break;
        if (sheepWords != bubbletext) break;
        sheepCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.30, 0.55)); // Original Random.Range(0.5, 2)
    }
    coupe = false;
    for(var i:int=0;i<10;i++){
    	if(coupe==true){
    		break;
    	}
    	yield(WaitForSeconds(0.15));  	
    }    
    sheepTalking = false;
    sheepBubbleAlpha=1;  
}

function playerSays (bubbletext:String)
{
	while (playerTalking == true || botTalking == true || sheepTalking == true) 
	{
		yield WaitForSeconds (0.1);
	}
	
    playerTalking = true;
    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
    bubbletext = parseCodes (bubbletext);
    playerWords = bubbletext;
	playerCurrentWords = "";

    for (var letter in bubbletext.ToCharArray ())
    {
        if (playerCurrentWords == bubbletext) break;
        if (playerWords != bubbletext) break;
        playerCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.01, 0.5)); // Original Random.Range(0.5, 2)
    } 
    playerTalking = false;
}
  
function parseCodes(parseText:String) : String
{

	// Ambar aime ca
	if (parseText.Contains ("CPlus"))
	{
		ambar ++;
		parseText = parseText.Replace ("CPlus", "");
		TrustBar.GetComponent (ProgressBar).changeState (ambar, maxAmbar);
		AureolesAmbar.GetComponent (aureoleSprite).aureoleAlphaSet(ambar, maxAmbar);
		AudioSource.PlayClipAtPoint(sfxTrust[Random.Range(0, sfxTrust.Length)], transform.position);
		if(ambar==maxAmbar){
			ambarWin = true;
		}
	}
	
	// Ambar aime pas ca
	if (parseText.Contains ("CMoins"))
	{
		ambar --;
		if(ambar<0){ambar=0;}
		parseText = parseText.Replace ("CMoins", "");		
		TrustBar.GetComponent (ProgressBar).changeState (ambar, maxAmbar);
		AureolesAmbar.GetComponent (aureoleSprite).aureoleAlphaSet(ambar, maxAmbar);
		AudioSource.PlayClipAtPoint(sfxError[Random.Range(0, sfxError.Length)], transform.position);
	}	

	// Mouton aime ca
	if (parseText.Contains ("CMouton"))
	{
		mouton ++;
		parseText = parseText.Replace ("CMouton", "");		
		PatienceBar.GetComponent (ProgressBar).changeState (mouton, maxMouton);
		AureolesMoutons.GetComponent (aureoleSprite).aureoleAlphaSet(mouton, maxAmbar);
		AudioSource.PlayClipAtPoint(sfxMoutons[Random.Range(0, sfxMoutons.Length)], transform.position);
		
		var alea:int = Random.value * 15;
		//print(alea);
		if(alea==1&&mouton1==false){
			sheepSays("Now you're talking.");
			mouton1 = true;
		}	
		if(alea==2&&mouton2==false){
			sheepSays("Amen to that.");
			mouton2 = true;
		}	
		if(alea==3&&mouton3==false){
			sheepSays("So be it");
			mouton3 = true;
		}	
		if(alea==4&&mouton4==false){
			sheepSays("Words of wisdom.");
			mouton4 = true;
		}	
		if(alea==5&&mouton5==false){
			sheepSays("Spoken like a prophet!");
			mouton4 = true;
		}
		if(mouton==maxMouton){
			moutonWin = true;
		}						
	}		

	// Gambit
	if (parseText.Contains ("CGambit"))
	{
		curGambits++;
		nuit.color.a = curGambits/maxGambits;
		
		alea = Random.value * 50;
		//print(alea);
		if(curGambits==15){
			alea = 50;
			botSays("The sun is going down, I'll have to head home soon, but...",true);
		}
		if(alea==1&&gambit1==false){
			sheepSays("I'm sure Ambar will want to change the subject now.");
			botSays("Shut up, insolent sheep.",true);
			gambit1 = true;
		}
		if(alea==2&&gambit2==false){
			sheepSays("Judging form Ambar's empty gaze, I think you've lost him with this one.");
			botSays("I'm not lost! Wait... you made me lose my train of thought.",true);
			gambit2 = true;
		}
		if(alea==3&&gambit3==false){
			sheepSays("I think that might be a bit complicated for Ambar.");
			botSays("See what I have to put up with?",true);
			botSays("Now what were we saying... hmmm...",true);
			gambit3 = true;
		}	
		if(alea==4&&gambit4==false){
			sheepSays("Hey Ambar, can we move to another pasture now?");
			botSays("My soul is at stake here, I have to take this seriously.",true);
			sheepSays("What about my soul?");
			botSays("Ha ha! Can you believe this sheep? Now where were we...",true);
			gambit4 = true;
		}		
		if(alea==5&&gambit5==false){
			botSays("Hmm... interesting.", true);
			sheepSays("I don't think Ambar actually understood that.");
			botSays("I did, but I'd rather talk of something else.",true);
			gambit5 = true;
		}			
			
		parseText = parseText.Replace ("CGambit", "");	
		if(curGambits>=maxGambits){
			noWin = true;
			parseText = "*yawn*";
		}
	}
	return parseText;
}
 
function pause(duree:int){
	coupe = false;
    for(var i:int=0;i<duree;i++){
    	if(coupe==true){
    		break;
    	}
    	yield(WaitForSeconds(0.1));  	
    }
}

function sendText(){
	userInput = userInput.Replace("\r","");
	userInput = userInput.Replace("\n","");
	playSound();
	consoleText = consoleText + "\n\n[Player]: " + userInput;        
	playerSays (userInput);
	postMessage (userInput, false, false);
	userInput = "";
}
function updateText(value:String){
	userInput = value;
}

function OnGUI ()
{
	GUI.enabled = true;
/*	if (!(playerTalking == true || botTalking == true || sheepTalking == true || waitingForMessage == true)&&(Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter)))
	{
		sendText();

	}
	else if (userInput.Contains("\r")||userInput.Contains("\n"))
	{
		sendText();
	}*/
	

	//GUI.skin.label.alignment = TextAnchor.UpperLeft; // Text alignment
	
	// 	User text bubble		
    var textwidth : float = userBubbleCoords.width * Screen.width / 1280;
    var bubbleheight : float = textBubbleStyle2.CalcHeight (GUIContent (playerWords), textwidth);
    var minWidth : float;
    var maxWidth : float;
    textBubbleStyle.CalcMinMaxWidth (GUIContent (playerWords), minWidth,  maxWidth);
    if (maxWidth > textwidth)
    {
    	maxWidth = textwidth;
    }  
    if (maxWidth < 100)
    	maxWidth = 100; 
    GUI.color.a = playerCurrentWords.Length;
    GUI.color.a /= 8;  
    if (playerCurrentWords == playerWords)
    	GUI.color.a = 1;
    GUI.Box (Rect (userBubbleCoords.x * Screen.width / 1280 + textwidth - maxWidth, userBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), playerCurrentWords, textBubbleStyle2);
    GUI.color.a = 1.0;
    
    // Bot text bubble
    textwidth = textBubbleCoords.width * Screen.width / 1280;
    bubbleheight = textBubbleStyle.CalcHeight (GUIContent (botWords), textwidth);
	textBubbleStyle.CalcMinMaxWidth (GUIContent (botWords), minWidth,  maxWidth);
    if (maxWidth > textwidth)
    {
    	maxWidth = textwidth;
    }    
    if (maxWidth < 100)
    	maxWidth = 100;
    GUI.color.a = botCurrentWords.Length;
    GUI.color.a /= 8;
    if (botCurrentWords == botWords)
    	GUI.color.a = 1;
    GUI.Box (Rect (textBubbleCoords.x * Screen.width / 1280, textBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), botCurrentWords, textBubbleStyle);
    GUI.color.a = 1.0;

    // Sheep text bubble
    textwidth = sheepBubbleCoords.width * Screen.width / 1280;
    bubbleheight = textBubbleStyle.CalcHeight (GUIContent (sheepWords), textwidth);
	textBubbleStyle.CalcMinMaxWidth (GUIContent (sheepWords), minWidth,  maxWidth);
    if (maxWidth > textwidth)
    {
    	maxWidth = textwidth;
    }    
    if (maxWidth < 100)
    	maxWidth = 100;
    if(sheepTalking){
	    GUI.color.a = sheepCurrentWords.Length;
	    GUI.color.a /= 8;
	    if (botCurrentWords == sheepWords)
	    	GUI.color.a = 1;
	    GUI.Box (Rect (sheepBubbleCoords.x * Screen.width / 1280, sheepBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), sheepCurrentWords, textBubbleStyle);
	    GUI.color.a = 1.0;   
    }
    else{
	    sheepBubbleAlpha = sheepBubbleAlpha - 0.01;
	    GUI.color.a = sheepBubbleAlpha;
	    GUI.Box (Rect (sheepBubbleCoords.x * Screen.width / 1280, sheepBubbleCoords.y * Screen.height / 800, maxWidth, bubbleheight), sheepCurrentWords, textBubbleStyle);
	    GUI.color.a = 1.0;    
    }

    
    // Buttons 
	GUILayout.BeginArea(Rect(10* Screen.width / 1280,762* Screen.height / 800,250* Screen.width / 1280,40* Screen.height / 800));
	GUILayout.BeginHorizontal();
	
	//Restart Button
	if (GUILayout.Button("[RESTART]", buttonStyle))
		{
			initConversation ();
			playSound();
		}

	//Log Button
	if (GUILayout.Button("[LOG]", buttonStyle))
		{
			showLog = !showLog; // Toggles log visibility
			playSound();
		}
			
	GUILayout.EndHorizontal();
	GUILayout.EndArea();
		
	//Fullscreen btn
	/*if (GUI.Button(new Rect (1240* Screen.width / 1280,720* Screen.height / 800,32 * Screen.width / 1280,32* Screen.height / 800), fullScreenIcon, labelStyle))
		{
		Screen.fullScreen = !Screen.fullScreen;
		}*/


						
	//Progress bars
	
	GUILayout.BeginArea(Rect(740* Screen.width / 1280,762* Screen.height / 800,540* Screen.width / 1280,40* Screen.height / 800));
	GUILayout.BeginHorizontal();
		GUILayout.Label ("AMBAR", labelStyle);
		GUILayout.Label ("SHEEP", labelStyle);	
	GUILayout.EndHorizontal();
	GUILayout.EndArea();	
			
	//Log	
    if (showLog) // Manages log window elements
	{
		GUILayout.BeginArea(Rect(logCoords.x*Screen.width/1280,logCoords.y*Screen.height/800,logCoords.width*Screen.width/1280,logCoords.height*Screen.height/800),logBoxStyle);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition);
				GUILayout.TextArea(consoleText, logTextStyle);	
			GUILayout.EndScrollView();
		GUILayout.EndArea();	
					
	}

	// Text box for user input
	if ((Event.current.type == EventType.MouseUp)&& (GUI.GetNameOfFocusedControl() == "inputbox") && (userInput == "[Type your answer here]"))
	{
			userInput = "";
	}
		
	GUI.SetNextControlName ("inputbox");
	
	// Disables the text field while Ambar is talking
	if(playerTalking == true || botTalking == true || sheepTalking == true || waitingForMessage == true){
		GUI.enabled = false;
	}
	
	//GUI.Label(Rect(inputBoxCoords.x*Screen.width/1280,inputBoxCoords.y*Screen.height/800,inputBoxCoords.width*Screen.width/1280,inputBoxCoords.height*Screen.height/800), "", inputBoxStyle);
//	SEND button
	/*if (GUI.Button(new Rect(sendButtonCoords.x*Screen.width/1280,sendButtonCoords.y*Screen.height/800,sendButtonCoords.width*Screen.width/1280,sendButtonCoords.height*Screen.height/800), "SEND", inputBoxStyle))
	{
		sendMessage=true;
	}	*/
	GUI.enabled = true;
	// If the max string length is limited (say, to 250), a phantom line-break is introduced into the text field. There does not seem to be a way to fix this.
}

function initConversation ()
{
	session++;
	userID = "id_" + userNumber + "_session_" + session;
	curGambits = 0;
	nuit.color.a = curGambits;	
	ambar = 0;
	mouton = 0;
	
	postMessage ("", false, false);	
	PatienceBar.GetComponent (ProgressBar).changeState (mouton, maxMouton);
	TrustBar.GetComponent (ProgressBar).changeState (ambar, maxAmbar);
	AureolesAmbar.GetComponent (aureoleSprite).aureoleAlphaSet(ambar, maxAmbar);
	AureolesMoutons.GetComponent (aureoleSprite).aureoleAlphaSet(mouton, maxMouton);
}
function endGameBof(){
	noWin = false;
	botSays("Well, it's getting late and this conversation is going nowhere.",true);
	botSays("Even the sheep here are getting tired and they're usually a good audience.",true);
	sheepSays("Yup, we like listening to prophets, but enough is enough.");
	botSays("There's some good stuff in your message there, but....",true);
	postMessage("what did I do wrong",true, false);
	botSays("So I think I'll pass this time.",true);
	botSays("You're welcome to try again once I've forgotten all about you.",true);


}
function endGameMouton(){
	moutonWin = false;
	sheepSays("We're in!");
	postMessage("sheepsayz : We're in",false,true);
	postMessage("ambarsayz : I'm a more complex person",false,true);
	AudioSource.PlayClipAtPoint(sfxEndGame[Random.Range(0, sfxEndGame.Length)], transform.position);
	botSays("It seems you've converted my sheep!",true);
	botSays("They're a good audience for prophets.",true);
	botSays("They appreciate clear, unambiguous truths.",true);
	botSays("I'm a more complex person.",true);
	sheepSays("Sure you are, Ambar.");
	
}

function endGameAmbar(){
	ambarWin=false;
	botSays("Hmmm...",true);
	sheepSays("I don't like the look in his eyes");
	botSays("I've seen the light!",true);
	AudioSource.PlayClipAtPoint(sfxEndGame[Random.Range(0, sfxEndGame.Length)], transform.position);
	botSays("Your new religion is all I ever hoped for!",true);
	botSays("I hope you like the company of sheep because we will follow you everywhere.",true);
	sheepSays("You don't know what you got yourself into.");
	postMessage("The player won",true, true);
	botSays("Lead me!",true);	
}

function getNewID ()
{
    waitingForId = true;
    var w = WWW (NewIDUrl);

    yield w;
    waitingForId = false;
    if (!String.IsNullOrEmpty (w.error))
    {
       botOutput = "I have a problem communicating with my brain, please try talking to me later.";
       botSays (botOutput,false);
    }
    else 
    {
    	userNumber = int.Parse (w.text); // Retrieve bot response for display in text bubble
	}    
    //print ("userNumber:" + userNumber);
}

function postMessage (message : String, pause:boolean, silent:boolean):IEnumerator
{ 
    message = message.Replace (":", "");	// avoid users sending commands to server
    if(message==""){message="bob";}
    //=================================Translation Code====================================================
    //ACCESS CODE FROM TRANSLATION SCRIPT
    //===============================End of translation code===============================================
    var msgURL = ChatScripUrl + "message=" + WWW.EscapeURL (message) + "&userID=" + WWW.EscapeURL (userID);
    Debug.Log(msgURL);
    var w = WWW (msgURL);
    waitingForMessage = true;
    while(w.isDone!=true){
    	yield WaitForSeconds(0.1);
    }
    waitingForMessage = false;

    if (!String.IsNullOrEmpty (w.error))
    {
       botOutput = "I have a problem communicating with my brain, please try talking to me later.";
       botSays (botOutput,false);
    }
    else 
    {
    	botOutput = w.text; // Retrieve bot response for display in text bubble
		if(!silent){botSays (botOutput,pause);}
	}
}

// Button sounds	
function playSound() {
	if (sfxAction.Length >= 1 && sfxAction[0]) {
	AudioSource.PlayClipAtPoint(sfxAction[Random.Range(0, sfxAction.Length)], transform.position);
	}
}

function loadMenu(){
    Application.LoadLevel("MainMenu");
}