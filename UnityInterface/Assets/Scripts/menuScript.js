﻿#pragma strict

private var botOutput : String = null;
private var consoleText : String;
private var scrollPosition : Vector2 = Vector2.zero;

// Text bubble variables
public var textDelay = 0.2;
private var botWords : String = "Testing, testing, one two, one two";
private var botCurrentWords : String = "";
private var coupe : boolean = false;

// Flags
private var botTalking : boolean = false;


// GUI variables
public var textBubbleStyle : GUIStyle;
public var textBubbleCoords : Rect;

public var centeredWidth : boolean = false;
public var verticalAlign :  boolean = false;

private var groupPosX : float = 0;
private var groupPosY : float = 0;

private var btnLength : int = Screen.width/20;
private var btnHeight : int = Screen.height/20;
private var btnSpacing : int = Screen.width/10;

private var playBtn : boolean;
private var tutorialBtn : boolean;
private var creditsBtn : boolean;
private var introBtn:boolean;

var labelStyle : GUIStyle;
var fullScreenIcon : Texture;

public var menuStyle : GUIStyle;
menuStyle.fontSize = Screen.width/50;

// User agent
private var chromeBrowser : boolean = false;

//Sounds
public var selectSfxNumber : AudioClip[]; //Array to make a random bank of sound variations. Use with AudioSource.PlayClipAtPoint(selectSfxNumber[Random.Range(0, selectSfxNumber.Length)], transform.position);

function Start ()
{	
	iTween.CameraFadeAdd();
  #if UNITY_WEBPLAYER
    testBrowser();
  #endif

}

function Update(){
	if(Input.GetMouseButtonDown(0)&&(botCurrentWords.Length>3)){
		botCurrentWords = botWords;
		coupe = true;
	}
}

function botSays (bubbletext : String)
{
    bubbletext = bubbletext.Replace ("\r", "");
    bubbletext = bubbletext.Replace ("\n", "");
	while (botTalking == true)
	{
		yield WaitForSeconds (0.1);
	}
	
    botTalking = true;
    botWords = bubbletext;
	botCurrentWords = "";
	

    for (var letter in bubbletext.ToCharArray ())
    {
        if (botCurrentWords == bubbletext) break;
        if (botWords != bubbletext) break;
        botCurrentWords += letter;
        yield WaitForSeconds (textDelay * Random.Range (0.25, 0.5)); // Original Random.Range(0.5, 2)
    }
    
    coupe = false;
    for(var i:int=0;i<20;i++){
    	if(coupe==true){
    		break;
    	}
    	yield(WaitForSeconds(0.1));  	
    }
    botTalking = false;
    

}

// All buttons sound	
function playSound() {
	if (selectSfxNumber.Length >= 1 && selectSfxNumber[0]) {
	AudioSource.PlayClipAtPoint(selectSfxNumber[Random.Range(0, selectSfxNumber.Length)], transform.position);
	}
}
  
function OnGUI ()
{
    // Bot text bubble
	GUI.skin.label.alignment = TextAnchor.MiddleCenter; // Text alignment
	var textWidth : float;
	var bubbleHeight : float;
    var minWidth : float;
    var maxWidth : float;
    
    textWidth = textBubbleCoords.width * Screen.width / 1280;
    bubbleHeight = textBubbleStyle.CalcHeight (GUIContent (botWords), textWidth);
	textBubbleStyle.CalcMinMaxWidth (GUIContent (botWords), minWidth,  maxWidth);
    if (maxWidth > textWidth)
    {
    	maxWidth = textWidth;
    }    
    if (maxWidth < 100)
    	maxWidth = 100;
    	
    GUI.color.a = botCurrentWords.Length;
    GUI.color.a /= 8;
    if (botCurrentWords == botWords)
    	GUI.color.a = 1;
    GUI.Box (Rect (textBubbleCoords.x * Screen.width / 1280, textBubbleCoords.y * Screen.height / 800, maxWidth, bubbleHeight), botCurrentWords, textBubbleStyle);
    GUI.color.a = 1.0;
    //Disable buttons while bot is talking
    GUI.enabled = !botTalking;

if ( centeredWidth )
		groupPosX = Screen.width/4;
	if (!verticalAlign) {
		// All buttons are adjusted to the group. (0,0) is the topleft corner of the group.		
		GUI.BeginGroup (new Rect (groupPosX, Screen.height - Screen.height/20, Screen.width, Screen.height));
		playBtn = GUI.Button(new Rect (btnSpacing,0,btnLength,btnHeight), "  [PLAY]  ", menuStyle);
		introBtn = GUI.Button(new Rect (btnSpacing+btnLength*1.75,0,btnLength,btnHeight), "  [INTRO]  ", menuStyle);
		tutorialBtn = GUI.Button(new Rect (btnSpacing*2+btnLength*1.75,0,btnLength,btnHeight), "[TUTORIAL]", menuStyle);
		creditsBtn = GUI.Button(new Rect (btnSpacing*3+btnLength*1.75,0,btnLength,btnHeight), "[CREDITS] ", menuStyle);
		GUI.EndGroup ();
	}
	if (verticalAlign) {
		GUI.BeginGroup (new Rect (btnLength, btnHeight, Screen.width, Screen.height));
		playBtn = GUI.Button(new Rect (0,0,btnLength*2,btnHeight), "[PLAY]", menuStyle);
		tutorialBtn = GUI.Button(new Rect (0,btnHeight,btnLength*2,btnHeight), "[TUTORIAL]", menuStyle);
		creditsBtn = GUI.Button(new Rect (0,btnHeight*2,btnLength*2,btnHeight), "[CREDITS]", menuStyle);
		GUI.EndGroup ();
	}
	
	
	//Play Button
	if (playBtn)
	{
		playSound();
		botTalking = false;
		iTween.CameraFadeTo(iTween.Hash("time", 1.5f, "alpha", 1,"onComplete","loadMain","onCompleteTarget",gameObject));
		// See http://forum.unity3d.com/threads/itween-camerafade.64104/#post-424538
	}
	
	//Tutorial Button
	if (introBtn)
	{
		playSound();
		botSays("SimProphet is a very ulmash game in which you are the prophet.");
		botSays("A 'choose your own religion game' if you prefer.");
		botSays("The context is you've just met a divinity and are on a mission to spread a new faith.");
		botSays("The first person you stumble upon is Ambar, a humble Babylonian shepherd.");
		botSays("It is your duty to inform him of your god's message in the hope of making him a follower.");
		botSays("Press the [play] button to get started or [tutorial] for more technical information");
	}
				
	//Tutorial Button
	if (tutorialBtn)
	{
		playSound();
		botSays("Talking to Ambar is simple...");
		botSays("Just talk to him in plain English!");
		botSays("He might not understand everything, though.");
		botSays("But don't let that discourage you.");
		botSays("The progress bars at the bottom of the screen will inform you of whatever progress you're making.");
		botSays("At some point Ambar will either join your cult willfully or grow tired of the discussion.");
	}
	
	//Credits Button
	if (creditsBtn)
	{
		playSound();
		botSays("SimProphet was developed at the lablablab (lablablab.net).");
		botSays("Lead design, writing, scripting and code by Jonathan Lessard.");
		botSays("Additionnal design, writing, scripting and code by Samuel Cousin.");
		botSays("Illustrations by Olivia Colden.");
		botSays("Chatscript engine by Bruce Wilcox.");
	}		
	//Fullscreen btn
	/*if (GUI.Button(new Rect (1240* Screen.width / 1280,720* Screen.height / 800,32 * Screen.width / 1280,32* Screen.height / 800), fullScreenIcon, labelStyle))
	{
	Screen.fullScreen = !Screen.fullScreen;
	}*/
	
	/*if (chromeBrowser) {
		playSound();
		botSays("Seems like you're using Chrome.");
		botSays("You might have to refresh your browser window to speak with Ambar.");
		chromeBrowser = !chromeBrowser;
	}*/
	
	if (Event.current.isKey || Event.current.isMouse) {
		if (!botTalking && botCurrentWords == botWords) {
		botCurrentWords = "";
		}	
	}
	
};

function loadMain(){
    Application.LoadLevel("Main");
}

// Fire the isBrowserChrome() function in the webpage
function testBrowser() {
	Application.ExternalEval ("isBrowserChrome()");
}

// Is called back only if isBrowserChrome() is true
function browserIsChrome( param : String ) {
	chromeBrowser = !chromeBrowser;
}
